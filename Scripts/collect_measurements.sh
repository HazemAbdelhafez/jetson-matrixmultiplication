#! /bin/bash

if [ $(id -u) != 0 ]; then
	echo "This script requires root permissions"
	echo "$ sudo "$0""
	exit
fi

wattsup="/home/ubuntu/watts-up/wattsup /dev/ttyUSB0"
cpu_frequencies="102000 204000 307200 403200 518400 614400 710400 825600 921600 1036800 1132800 1224000 1326000 1428000 1555500 1632000 1734000"

cpu_info() {
	cat /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable
	cat /sys/devices/system/cpu/cpu0/active
	cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
	cat /sys/devices/system/cpu/cpu1/online
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
	cat /sys/devices/system/cpu/cpu2/online
	cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
	cat /sys/devices/system/cpu/cpu3/online
	cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor
	cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed
}

config_cpu_cores() {
	num_cpus=$1
	if [ $num_cpus -eq 1 ]
	then
		# echo "Turning on CPU 1"
		echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable
		echo 0 > /sys/devices/system/cpu/cpu1/online
		echo 0 > /sys/devices/system/cpu/cpu2/online
		echo 0 > /sys/devices/system/cpu/cpu3/online
	elif [ $num_cpus -eq 2 ]
	then
		# echo "Turning on CPU 1 & 2"
		echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable
		echo 1 > /sys/devices/system/cpu/cpu1/online
		echo 0 > /sys/devices/system/cpu/cpu2/online
		echo 0 > /sys/devices/system/cpu/cpu3/online
	elif [ $num_cpus -eq 3 ]
	then
		# echo "Turning on CPU 1, 2 & 3"
		echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable
		echo 1 > /sys/devices/system/cpu/cpu1/online
		echo 1 > /sys/devices/system/cpu/cpu2/online
		echo 0 > /sys/devices/system/cpu/cpu3/online
	elif [ $num_cpus -eq 4 ]
	then
		# echo "Turning on CPU 1, 2, 3 & 4"
		echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable
		echo 1 > /sys/devices/system/cpu/cpu1/online
		echo 1 > /sys/devices/system/cpu/cpu2/online
		echo 1 > /sys/devices/system/cpu/cpu3/online
	else
		echo "Invalid number of cores!"
		exit 1
	fi
}

config_cpu_frequency() {
	num_cpus=$1
	cpu_frequency=$2

	# echo "Setting CPU frequency to $cpu_frequency"

	if [ $num_cpus -ge 1 ]
	then
		# echo "Setting CPU 1 frequency to $cpu_frequency"
		echo "userspace" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
		echo $cpu_frequency > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed
	fi

	if [ $num_cpus -ge 2 ]
	then
		# echo "Setting CPU 2 frequency to $cpu_frequency"
		echo "userspace" > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
		echo $cpu_frequency > /sys/devices/system/cpu/cpu1/cpufreq/scaling_setspeed
	fi

	if [ $num_cpus -ge 3 ]
	then
		# echo "Setting CPU 3 frequency to $cpu_frequency"
		echo "userspace" > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
		echo $cpu_frequency > /sys/devices/system/cpu/cpu2/cpufreq/scaling_setspeed
	fi

	if [ $num_cpus -ge 4 ]
	then
		# echo "Setting CPU 4 frequency to $cpu_frequency"
		echo "userspace" > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor
		echo $cpu_frequency > /sys/devices/system/cpu/cpu3/cpufreq/scaling_setspeed
	fi
}

matrixmultiply() {
	num_cpus=$1
	num_threads=$2
	cpu_frequency=$3
	repeat=11
	dimensions=1024
	settings="c$num_cpus-t$num_threads-f$cpu_frequency-d$dimensions"
	result="matrixmultiply-$settings"
	result_file="./r/$result"
	wattsup_file="./r/wattsup-$settings"
	wattsup_pid=-1

	# cpu_info

	if [ -e $result_file ]
	then
		echo "Skipping $result_file. Result file already exists!"
	else
		echo $result

		if [ -e $wattsup_file ]
		then
			echo "Skipping $wattsup_file. Wattsup file already exists!"
		else
			$wattsup > $wattsup_file &
			wattsup_pid=$!
		fi

		./bin/mb 0 $num_threads $repeat 4 $dimensions > $result_file

		if [ $wattsup_pid -ne -1 ]
		then
			echo "Killing $wattsup_pid!"
			kill $wattsup_pid
		fi
	fi
}

# Hide errors!
exec 2> /dev/null

echo "Setting CPUs to maximum!"
/jetson/scripts/maxCPU.sh

echo "Starting benchmark!"

for i in {1..4}
do
	config_cpu_cores $i

	max_threads=$i

	j=1
	while [ $j -le $max_threads ]
	do

		for k in $cpu_frequencies
		do
			config_cpu_frequency $i $k
			matrixmultiply $i $j $k
		done

		let j+=1
	done
done

echo "Setting CPUs to maximum!"
/jetson/scripts/maxCPU.sh
