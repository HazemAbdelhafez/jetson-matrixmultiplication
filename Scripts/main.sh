#! /bin/bash

if [ $(id -u) != 0 ]; then
	echo "This script requires root permissions"
	echo "$ sudo "$0" username repeats"
	exit
fi


# Directories
username=$1


wattsup="/home/$username/watts-up/wattsup /dev/ttyUSB0"
GLOBAL_DIR="/home/$username/matrix-multiplication-run"
SCRIPTS_DIR="/home/$username/cuda-workspace/GPUMatMul/Scripts"
matmul_script="$SCRIPTS_DIR/matmul.sh"
results_dir="$GLOBAL_DIR/results"
bin_dir="/home/$username/cuda-workspace/GPUMatMul/Release"
if [ ! -d "$GLOBAL_DIR" ]; then
  # Create the parent results directory if it doesn't exist
  mkdir $GLOBAL_DIR
fi



# Parameters
benchmarks="1 2 3 4 5 6 7 8 9 10 11"
num_of_cores="4"
num_of_threads="4"
gpu_frequencies="76800000 153600000 230400000 307200000 384000000 460800000 537600000 614400000 691200000 768000000 844800000 921600000 998400000"
cpu_frequencies="102000 204000 307200 403200 518400 614400 710400 825600 921600 1036800 1132800 1224000 1326000 1428000 1555500 1632000 1734000"
# gpu_frequencies=998400000
# cpu_frequencies=1734000
mat_size="64 128 256 512 1024 2048 4096"
cpu_cols="16 32 64 128 256 512 1024 2048 4096"

# Constants
repeats=$2
verbose=0
sleep_val=3
set_cpu_freq(){
	echo "Setting cores to frequency $1"
	cpu_frequency=$1
	echo "userspace" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
	echo $cpu_frequency > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed

}
set_gpu_freq(){

	echo "Setting GPU frequency to " $1
	echo $1 > /sys/kernel/debug/clock/override.gbus/rate
	echo 1 > /sys/kernel/debug/clock/override.gbus/state
	ret=$?
	if [ ${ret} -ne 0 ]; then
		echo "Error: Failed to max GPU frequency!"
	fi
}
set_num_cores(){
echo "Setting number of cores to " $1

cpu0=`cat  /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable`
cpu1=`cat /sys/devices/system/cpu/cpu1/online`
cpu2=`cat /sys/devices/system/cpu/cpu2/online`
cpu3=`cat /sys/devices/system/cpu/cpu3/online`

num_cpus=$1
if [ $num_cpus -eq 1 ]
then
	# echo "Turning on CPU 1"
	if [ $cpu0 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable; fi
	if [ $cpu1 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpu1/online; fi
	if [ $cpu2 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpu2/online; fi
	if [ $cpu3 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpu3/online; fi
	for i in `ls /sys/devices/system/cpu/cpu*/cpuidle/state*/disable`; do echo 0 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu0/cpuidle/state*/disable`; do echo 1 > $i; done
elif [ $num_cpus -eq 2 ]
then
	# echo "Turning on CPU 1 & 2"
	if [ $cpu0 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable; fi
	if [ $cpu1 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu1/online; fi
	if [ $cpu2 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpu2/online; fi
	if [ $cpu3 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpu3/online; fi
	for i in `ls /sys/devices/system/cpu/cpu*/cpuidle/state*/disable`; do echo 0 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu0/cpuidle/state*/disable`; do echo 1 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu1/cpuidle/state*/disable`; do echo 1 > $i; done
elif [ $num_cpus -eq 3 ]
then
	# echo "Turning on CPU 1, 2 & 3"
	if [ $cpu0 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable; fi
	if [ $cpu1 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu1/online; fi
	if [ $cpu2 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu2/online; fi
	if [ $cpu3 -eq 0 ]; then echo 0 > /sys/devices/system/cpu/cpu3/online; fi
	for i in `ls /sys/devices/system/cpu/cpu3/cpuidle/state*/disable`; do echo 0 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu0/cpuidle/state*/disable`; do echo 1 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu1/cpuidle/state*/disable`; do echo 1 > $i; done
	for i in `ls /sys/devices/system/cpu/cpu2/cpuidle/state*/disable`; do echo 1 > $i; done
elif [ $num_cpus -eq 4 ]
then
	# echo "Turning on CPU 1, 2, 3 & 4"
	if [ $cpu0 -eq 1 ]; then echo 0 > /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable; fi
	if [ $cpu1 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu1/online; fi
	if [ $cpu2 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu2/online; fi
	if [ $cpu3 -eq 0 ]; then echo 1 > /sys/devices/system/cpu/cpu3/online; fi
	for i in `ls /sys/devices/system/cpu/cpu*/cpuidle/state*/disable`; do echo 1 > $i; done
else
	echo "Invalid number of cores!"
	exit 1
fi
}

set_general_settings(){
	# Set fan speed to max
	FAN_SPEED=255
	echo $FAN_SPEED > /sys/kernel/debug/tegra_fan/target_pwm

	# Set EMC settings to max performance
	EMC_MAX_FREQ=`cat /sys/kernel/debug/clock/override.emc/max`
	echo $EMC_MAX_FREQ > /sys/kernel/debug/clock/override.emc/rate
	echo 1 > /sys/kernel/debug/clock/override.emc/state
}
create_benchmark_results_dir(){
echo "Creating benchmark results directory for benchmark " $1
if [ ! -d "$results_dir" ]; then
  # Create the parent results directory if it doesn't exist
  mkdir $results_dir
fi

if [ -d "$results_dir" ]; then
  # Create the benchmark result directory if the parent folder is created successfuly.
	if [ ! -d "$1" ]; then
		mkdir $results_dir/$1
	fi
fi

}

run(){
	prefix="$results_dir/$benchmark"
	postfix="$cores-$threads-$repeats-$gpufreq-$cpufreq-$size-$cols"
	wattsup_file="$prefix/power-$postfix"

	$wattsup > $wattsup_file &
	wattsup_pid=$!

	echo "Matrix size $size x $size, CPU columns: $cols"
	flops_time=`bash $matmul_script $verbose $repeats $benchmark $threads $size $cols $bin_dir`
	echo "GFLOPS - Runtime : $flops_time"
	result_file="$prefix/result-$postfix"

	# Report the results
	echo $flops_time > $result_file
	if [ $wattsup_pid -ne -1 ];
	then
		kill $wattsup_pid > /dev/null
	fi
}

query_cpu_settings(){

	cpu0=`cat  /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable`
	if [ $cpu0 -eq 0 ]; then cpu0=1;fi
	cpu1=`cat /sys/devices/system/cpu/cpu1/online`
	cpu2=`cat /sys/devices/system/cpu/cpu2/online`
	cpu3=`cat /sys/devices/system/cpu/cpu3/online`
	echo "Cores status: $cpu0 $cpu1 $cpu2 $cpu3"

	governer=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor`
	echo "CPU frequency governer: $governer"

	freq=`cat  /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed`
	echo "CPU current frequency: $freq"
}
query_gpu_settings(){
	gpustate=`cat /sys/kernel/debug/clock/override.gbus/state`
	gpurate=`cat /sys/kernel/debug/clock/override.gbus/rate`
	echo "GPU state: $gpustate"
	echo "GPU rate: $gpurate"
}
query_general_settings(){
	fanspeed=`cat /sys/kernel/debug/tegra_fan/target_pwm`
	echo "Fan speed: $fanspeed"

	emcstatus=`cat /sys/kernel/debug/clock/override.emc/state`
	emcrate=`cat /sys/kernel/debug/clock/override.emc/rate`
	echo "EMC status: $emcstatus"
	echo "EMC rate: $emcrate"
}

main(){
echo "Running main script"
# Hide errors
exec 2> /dev/null

set_general_settings
query_general_settings

# Set number of cores
for cores in $num_of_cores
do
	set_num_cores $cores
	threads=$cores
	# sleep $sleep_val
	# Set GPU frequency
	for gpufreq in $gpu_frequencies
	do
		set_gpu_freq $gpufreq
		# sleep $sleep_val
		# Set CPU core freq
		for cpufreq in $cpu_frequencies
		do
			set_cpu_freq $cpufreq
			sleep $sleep_val
			query_cpu_settings
			query_gpu_settings
			for benchmark in $benchmarks
			do
				create_benchmark_results_dir $benchmark
				for size in $mat_size
				do	# CPU benchmarks: set CPU cols to be the same as size
					# if [ $size -lt 512 ]; then repeats=10000;else repeats=$2;fi
					if [ "$benchmark" -lt "4" ]
					then
				 		cols=$size
						run
					# GPU benchmakrs: set CPU cols to be zero
				elif [ "$benchmark" -lt "7" ]
					then
						cols=0
						run
					# Heterogeneous benchmarks: set CPU cols from a list of predefined values
					else
						for cols in $cpu_cols
						do
							if [ "$cols" -eq "$size" ]
							then
								break
							else
								run
						  fi
						done
					fi
				done
			done
		done
	done
done

}

main
