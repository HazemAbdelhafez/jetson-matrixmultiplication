#! /bin/bash
# Args: verbose repeats benchmark threads matSize cpuCols postfix bin_dir
verbose=$1
repeats=$2
benchmark=$3
threads=$4
dim=$5
cpucols=$6
bin_dir=$7

$bin_dir/GPUMatMul $verbose $repeats $benchmark $threads $dim $cpucols
# echo 55 66 $verbose $repeats $benchmark $threads $dim $cpucols
# echo 55 66
